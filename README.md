# Fedora Mindshare Committee

The _Fedora Mindshare Committee_ represents the outreach leadership in Fedora.
Mindshare aims to help outreach teams to reach their targets in a more effective way, by unifying and sharing their working process through an optimized and standardized communication.
It consists of mostly appointed, but also elected members.

## What is this place?

The Fedora Mindshare Committee uses this repository to record ongoing work and to track issues which need a specific resolution.
Tickets are normally discussed during weekly meetings.

You can read more about the Fedora Mindshare Committee on the [Mindshare's docs' page](https://docs.fedoraproject.org/en-US/mindshare/)

## File a new ticket

To file a new ticket, open a [new issue](https://pagure.io/mindshare/new_issue).

## How to contact the Mindshare Committee

Other than this repository, you can also find the members of the Fedora Mindshare Committee in the following places:

* Mailing list: mindshare@lists.fedoraproject.org
* IRC channel: #fedora-mindshare on Libera.chat ([webchat](https://web.libera.chat/?channels=#fedora-mindshare))
